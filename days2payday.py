from datetime import date
import calendar

# get tuple of first day in month and days in month
range = calendar.monthrange(date.today().year, date.today().month)

# Get days in month
days_in_month = range[-1]

# Define our payday period in a month
paydays=(15, days_in_month)

def days_to_payday(paydays):
  for payday in paydays:

    # Payday falls on weekdays, calendar.weekday() returns 0 Monday - 6 Sunday
    # If payday falls on saturday or sunday, adjust days down to match whatever day is Friday
    day_of_week = calendar.weekday(date.today().year, date.today().month, payday)

    if day_of_week >= 5:
      payday = payday - (day_of_week - 4)

    if payday >= date.today().day:
      days_until_payday = payday - date.today().day

      return days_until_payday

actual = days_to_payday(paydays)

if actual == 0:
  print "Check your account, payday is today!"

if actual > 0:
  print "Days until payday : %d" % (actual)
